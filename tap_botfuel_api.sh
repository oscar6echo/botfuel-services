
# Export vars in .env into shell
export $(egrep -v '^#' .env | xargs)

echo "APP_ID: $APP_ID"
echo "APP_KEY: $APP_KEY"

PROXY="http://$PROXY_LOGIN:$PROXY_PASSWORD_URL_ENCODED@$PROXY_HOST:$PROXY_PORT"
echo "proxy: $PROXY"

echo ' '
echo 'spellchecking'
curl -X GET 'https://api.botfuel.io/nlp/spellchecking?sentence=the+sentnece+yuo+watn+to+chekc&key=EN_1' \
-x $PROXY \
-k \
-H "App-Id: $APP_ID" \
-H "App-Key: $APP_KEY"

echo ' '
echo ' '
echo 'entity extraction'
curl -X GET 'https://api.botfuel.io/nlp/entity-extraction?sentence=two+liters+of+water+cost+4.02+pounds%2C+you+can+claim+50%25+discount+on+sunday&antidimensions=percentage,time,number' \
-x $PROXY \
-k \
-H "App-Id: $APP_ID" \
-H "App-Key: $APP_KEY"

echo ' '
echo ' '
echo 'qna'
curl -X POST 'https://api.botfuel.io/qna/api/v1/bots/classify' \
-H 'Content-Type: application/json' \
-x $PROXY \
-k \
-H "App-Id: $APP_ID" \
-H "App-Key: $APP_KEY" \
-d '{ "sentence": "Ce chauffeur est fou !!" }'
