
// patch https://github.com/axios/axios/pull/1232
const axios = require('axios');

const config = require('./config').config;


let handleSuccess = (response) => {
	console.log('*******************SUCCESS:');
	console.log('data:');
	console.log(response.data);
	console.log('status:');
	console.log(response.status);
	console.log('statusText:');
	console.log(response.statusText);
	console.log('headers:');
	console.log(response.headers);
	// console.log('config:');
	// console.log(response.config);
};

let handleError = (error) => {
	console.log('*******************ERROR:');
	if (error.response) {
		// The request was made and the server responded with a status code
		// that falls out of the range of 2xx
		console.log(error.response.data);
		console.log(error.response.status);
		console.log(error.response.headers);
	} else if (error.request) {
		// The request was made but no response was received
		// `error.request` is an instance of XMLHttpRequest in the browser and an instance of
		// http.ClientRequest in node.js
		console.log(error.request);
	} else {
		// Something happened in setting up the request that triggered an Error
		console.log('Error', error.message);
	}
	console.log(error.config);

};


async function asyncSpellchecking(sentence, key) {
	let requestConfig = {
		baseURL: 'https://api.botfuel.io/',
		url: 'nlp/spellchecking',
		method: 'get',
		headers: {
			'Content-Type': 'application/json',
			'App-Id': config.botfuelAppId,
			'App-Key': config.botfuelAppKey,
		},
		params: {
			sentence: sentence,
			key: key
		}
	};
	if (config.PROXY_BOOL) {
		requestConfig.proxy = config.proxy;
	}
	let response = await axios.request(requestConfig);
	return response;
};

async function asyncEntityExtraction(sentence) {
	let requestConfig = {
		baseURL: 'https://api.botfuel.io/',
		url: 'nlp/entity-extraction',
		method: 'get',
		headers: {
			'Content-Type': 'application/json',
			'App-Id': config.botfuelAppId,
			'App-Key': config.botfuelAppKey,
		},
		params: {
			sentence: sentence
		}
	};
	if (config.PROXY_BOOL) {
		requestConfig.proxy = config.proxy;
	}
	let response = await axios.request(requestConfig);
	return response;
};

async function asyncQnA(sentence) {
	let requestConfig = {
		baseURL: 'https://api.botfuel.io/',
		url: 'qna/api/v1/bots/classify',
		method: 'post',
		headers: {
			'Content-Type': 'application/json',
			'App-Id': config.botfuelAppId,
			'App-Key': config.botfuelAppKey,
		},
		data: {
			sentence: sentence
		}
	};
	if (config.PROXY_BOOL) {
		requestConfig.proxy = config.proxy;
	}
	let response = await axios.request(requestConfig);
	return response;
};


asyncSpellchecking('the sentnece yuo watn to chekc', 'EN_1')
	.then(handleSuccess)
	.catch(handleError);


asyncEntityExtraction('two liters of water cost 4.02 pounds, you can claim 50% discount on sunday')
	.then(handleSuccess)
	.catch(handleError);


asyncQnA('Ce chauffeur est fou !!')
	.then(handleSuccess)
	.catch(handleError);

