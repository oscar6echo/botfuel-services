
// patch https://github.com/axios/axios/pull/1232
import axios from 'axios';

import { config } from './config';
import cb from './callback';



async function asyncSpellchecking(sentence, key) {
	let requestConfig = {
		baseURL: 'https://api.botfuel.io/',
		url: 'nlp/spellchecking',
		method: 'get',
		headers: {
			'Content-Type': 'application/json',
			'App-Id': config.botfuelAppId,
			'App-Key': config.botfuelAppKey,
		},
		params: {
			sentence: sentence,
			key: key
		},
	};
	if(config.PROXY_BOOL) {
		requestConfig.proxy = config.proxy;
	}
let response = await axios.request(requestConfig);
	return response;
};

async function asyncEntityExtraction(sentence) {
	let requestConfig = {
		baseURL: 'https://api.botfuel.io/',
		url: 'nlp/entity-extraction',
		method: 'get',
		headers: {
			'Content-Type': 'application/json',
			'App-Id': config.botfuelAppId,
			'App-Key': config.botfuelAppKey,
		},
		params: {
			sentence: sentence
		},
	};
	if (config.PROXY_BOOL) {
		requestConfig.proxy = config.proxy;
	}
	let response = await axios.request(requestConfig);
	return response;
};

async function asyncQnA(sentence) {
	let requestConfig = {
		baseURL: 'https://api.botfuel.io/',
		url: 'qna/api/v1/bots/classify',
		method: 'post',
		headers: {
			'Content-Type': 'application/json',
			'App-Id': config.botfuelAppId,
			'App-Key': config.botfuelAppKey,
		},
		data: {
			sentence: sentence
		},
	}
	if (config.PROXY_BOOL) {
		requestConfig.proxy = config.proxy;
	}
	let response = await axios.request(requestConfig);
	return response;
};


asyncSpellchecking('the sentnece yuo watn to chekc', 'EN_1')
	.then(cb.handleSuccess)
	.catch(cb.handleError);


asyncEntityExtraction('two liters of water cost 4.02 pounds, you can claim 50% discount on sunday')
	.then(cb.handleSuccess)
	.catch(cb.handleError);


asyncQnA('Ce chauffeur est fou !!')
	.then(cb.handleSuccess)
	.catch(cb.handleError);
