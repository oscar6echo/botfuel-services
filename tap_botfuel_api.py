
import os
import json
import urllib

import requests as rq

from dotenv import load_dotenv, find_dotenv


load_dotenv(dotenv_path=find_dotenv())

headers = {
    'app-id': '7b433767',
    'app-key': '8c1a764a4c9ebf0ff56aeb755aed2dc9',
    'Content-Type': 'application/json'
}

# spellchecking
params = {
    'sentence': 'the sentnece yuo watn to chekc',
    'key': 'EN_1'
}

dic = {
    'login': os.getenv('PROXY_LOGIN'),
    'pwd': urllib.parse.quote(os.getenv('PROXY_PASSWORD')),
    'proxy_host': os.getenv('PROXY_HOST'),
    'proxy_port': os.getenv('PROXY_PORT'),
}
if os.getenv('PROXY_PASSWORD') is not None:
    dic['pwd'] = urllib.parse.quote(os.getenv('PROXY_PASSWORD'))

proxies = {
    'http': 'http://{login}:{pwd}@{proxy_host}:{proxy_port}'.format(**dic),
    'https': 'https://{login}:{pwd}@{proxy_host}:{proxy_port}'.format(**dic)
}

print(proxies)

r = rq.get('https://api.botfuel.io/nlp/spellchecking/',
           headers=headers,
           params=params,
           proxies=proxies)

res = r.content.decode('utf-8')
res = json.loads(res)
print('\nspellchecking')
print(json.dumps(res, indent=2))

# entity extraction
params = {
    'sentence': 'two liters of water cost 4.02 pounds, you can claim 50% discount on sunday'
}

r = rq.get('https://api.botfuel.io/nlp/entity-extraction/',
           headers=headers,
           params=params,
           proxies=proxies)

res = r.content.decode('utf-8')
res = json.loads(res)
print('\nentity extraction')
print(json.dumps(res, indent=2))


# qna
r = rq.post('https://api.botfuel.io/qna/api/v1/bots/classify',
            headers=headers,
            json={'sentence': 'Ce chauffeur est fou !!'},
            proxies=proxies)

res = r.content.decode('utf-8')
print('\nqna')
print(res)
