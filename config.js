
dotenv = require ('dotenv');
logging = require ('simple-node-logger');

const result = dotenv.config()
if (result.error) {
	throw result.error;
}

const log = logging.createSimpleLogger();

let config = {
	botfuelAppId: process.env.APP_ID,
	botfuelAppKey: process.env.APP_KEY,
	MicrosoftAppId: undefined,
	MicrosoftAppPassword: undefined,

	PROXY_HOST: process.env.PROXY_HOST,
	PROXY_LOGIN: process.env.PROXY_LOGIN,
	PROXY_PORT: process.env.PROXY_PORT,
	PROXY_PASSWORD: process.env.PROXY_PASSWORD,

	PROXY_BOOL: process.env.PROXY_BOOL == 'true' ? true : false
};

const proxy = config.PROXY_BOOL ? {
	host: config.PROXY_HOST,
	port: config.PROXY_PORT,
	auth: {
		username: config.PROXY_LOGIN,
		password: encodeURIComponent(config.PROXY_PASSWORD),
	}
} : undefined;

config.proxy = proxy;



log.info('config: ');
log.info(config);


module.exports = {
	config: config
};
