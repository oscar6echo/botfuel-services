# Testing botfuel services

## 1 - Introduction

We plan to request [botfuel services](https://app.botfuel.io/docs) 
+ spellchecking
+ entity extraction
+ Q&A

from clients implemented in:
+ Python (mostly 3.x)
+ Node 8.x (maybe 6.x)

So I tested clients in:
+ curl
+ Python 3.x
+ node 8.9.1+ (current) with CommonJS modules (.js)
+ node 8.9.1+ (current) with ES modules (.mjs)

The upload and download botfuel APIs are not tested here.  


## 2 - Run

From terminal:

```bash
# download
git clone https://gitlab.com/oscar6echo/botfuel-services.git

# move to directory
cd botfuel-services

# curl
. tap_botfuel_api.sh

# Python
python tap_botfuel_api.py

# node
npm install

# CJS
npm run js

# ESM
npm run mjs
```

## 3 - Proxy

To tap the API through a proxy, fill the file `.env_template` and rename it to `.env`.

> **WARNING**:  
As of mar18 the library **axios** needs be patched to work with a proxy.  
Cf. [axios PR#1232](https://github.com/axios/axios/pull/1232).

## 4 - Remarks on API

Methods change with endpoints:
+ Entity Extraction: GET
+ Spellchecking: GET
+ QnA (Classification): POST

